@RestResource(urlMapping='/Contacts/*')
global with sharing class ContactsCtrl {
  
  @HttpGet
  @RemoteAction
  global static List<Contact> getContacts() {
    List<Contact> contacts = [SELECT Id, Name FROM Contact LIMIT 10];
    return contacts;
  }

}